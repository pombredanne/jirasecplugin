/**
JiraSecPlugin 
Copyright 2016 Tosin Daniel Oyetoyan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package no.tosin.oyetoyan.machinelearning;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import no.tosin.oyetoyan.plugins.PropertyKeys;
import no.tosin.oyetoyan.plugins.SecurityKeyWords;
import no.tosin.oyetoyan.plugins.Validator;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.UnassignedDatasetException;

/**
 * @author tosindo
 *
 */
public class ProcessDataset {
	
	private List<TextDataStructure> textDataStructure;
	private List<String[]> featureSpace;
	private List<String> features;
	//private TextDataStructure textandClassification;
	private SecurityKeyWords skw;
	private Properties prop;
	private Set<String> termsFound;
	private Set<String> assetTermsFound;
	private Set<String> controlTermsFound;
	private Set<String> directTermsFound;
	private Set<String> indirectTermsFound;
	
	public ProcessDataset(SecurityKeyWords skw){
		
		this.skw = skw;
		prop = skw.getPropertyValues();
		
		//loaded keywords are used to represent the feature set
		Set<String> attributes = new HashSet<String>();
		attributes.addAll(skw.controlTerms());
		attributes.addAll(skw.directTerms());
		attributes.addAll(skw.indirectTerms());
		attributes.addAll(skw.piiTerms());
		//change to list to enforce order
		features = new ArrayList<String>(attributes);			
		
	}
	
	public void readDataset(String path, boolean hasHeader){
		
		String[] text_index = prop.getProperty(PropertyKeys.TEXT_INDEX).split(",");
		String separator = prop.getProperty(PropertyKeys.SEPARATOR);
		int class_index = 3;
		try{
			class_index = Integer.parseInt(prop.getProperty(PropertyKeys.CLASS_INDEX));
		}catch(NumberFormatException n){
			class_index = 3;
		}
		
		
		//index to integer
		int[] t_index = new int[text_index.length];
		for(int i=0; i<text_index.length; i++){
			try{
				t_index[i] = Integer.parseInt(text_index[i]);
			}catch(NumberFormatException n){
				//
			}
		}
		
		// excel or csv file?
		String ext = "csv";
		if(path.trim().endsWith("xlsx"))
			ext = "xlsx";
		if(path.trim().endsWith("csv"))
			ext = "csv";
		Set<String> text = new HashSet<String>();
		if(ext.equals("csv"))
			text = readText(path, hasHeader);
		else if(ext.equals("xlsx")){
			List<String> xcsv = new ExcelReader(path, separator).getDataAsCSV();
			if(hasHeader)
				xcsv.remove(0);
			text = new HashSet<String>(xcsv);
		}
		textDataStructure = new ArrayList<TextDataStructure>();
		
		//combine text
		for(String t : text){
			String[] tokens = t.split(separator);
			
			StringBuffer sb = new StringBuffer();
			//merge all text
			for(int index : t_index){
				sb.append(tokens[index-1]+" ");
			}

			//add to the data structure
			textDataStructure.add(new TextDataStructure(sb.toString(), tokens[class_index-1]));
		}
	}
	
	private Set<String> readText(String path, boolean header){
		
		Set<String> text = new HashSet<String>();
		
		try (BufferedReader br = new BufferedReader(new FileReader(path)))
		{
			int index = 0;
			String line = "";
			while((line=br.readLine()) != null){
				if(header && index==0){
					index++;
					continue;
				}					
				text.add(line);
				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return text;
	}

	public void addDatasetInstance(String text, String category){
		textDataStructure = new ArrayList<TextDataStructure>();
		textDataStructure.add(new TextDataStructure(text, category));
	}
	
	public void generateFeatureSpace(){
		
		featureSpace = new ArrayList<String[]>();
		termsFound = new HashSet<String>();
		//generate header
		String[] header = new String[features.size()+1];	//add 1 bcos of the category class
		//populate header with "v"+i as default values
		for(int i=0; i<header.length-1; i++){
			header[i] = "v"+i;
		}
		header[header.length-1] = "class";
		featureSpace.add(header);
		
		int method = 1;	//default method
		method = Validator.getInt(method, skw.getPropertyValues().getProperty(PropertyKeys.METHOD));
		
		//take the text, clean and tokenize
		for(int i=0; i<textDataStructure.size(); i++){
			TextDataStructure tds = textDataStructure.get(i);
			String[] attr = new String[features.size()+1];	//add 1 bcos of the category class
			//populate attr with "no" as default values
			for(int j=0; j<attr.length-1; j++){
				attr[j] = "no";
			}
			//add classification
			attr[attr.length-1] = tds.getClassification();
			
			Set<String> textTokens = cleanTextandTokenize(tds.getText());
			for(String token : textTokens){
				//return the position in the feature where the token is found
				int foundIndex = -1;
				switch(method){
					case 1:foundIndex = findMatchesWithDistanceAlgorithms(token, 1);
					break;
					
					case 2:foundIndex = findMatchesWithDistanceAlgorithms(token, 2);
					break;
					
					case 3:foundIndex = features.indexOf(token);
					break;
					
					default:foundIndex = features.indexOf(token);
					break;
				}
	
				if(foundIndex >= 0){
					attr[foundIndex] = "yes"; 	//change to yes if term is found in the feature set
					termsFound.add(token);
				}
			}
			
			featureSpace.add(attr);
		}
		
		//add specific keyword/term categories
		assetTermsFound = new HashSet<String>();
		assetTermsFound.addAll(skw.piiTerms());
		
		controlTermsFound = new HashSet<String>();
		controlTermsFound.addAll(skw.controlTerms());
		
		directTermsFound = new HashSet<String>();
		directTermsFound.addAll(skw.directTerms());
		
		indirectTermsFound = new HashSet<String>();
		indirectTermsFound.addAll(skw.indirectTerms());
		
		assetTermsFound.retainAll(termsFound);
		controlTermsFound.retainAll(termsFound);
		directTermsFound.retainAll(termsFound);
		indirectTermsFound.retainAll(termsFound);
		
	}
	
	private Set<String> cleanTextandTokenize(String text){
		
		text = StringUtils.stripAccents(text);
		text = StringUtils.lowerCase(text);
		text = text.replaceAll("[^a-zA-Z _-]"," ");
		String[] tokens = StringUtils.split(text);
		Set<String> t = new HashSet<String>();
		
		for (String token : tokens)
		{
			if(token.length() > 1)
				t.add(token);
		}
		
		return t;
	}
	
	/**
	 * @param token
	 * @param method
	 * Find similarities between the two words where the length > term_min_len with the LevenshteinDistance algorithm
	 * LevenshteinDistance: This is the number of changes needed to change one String into another,
	 * where each change is a single character modification (deletion, insertion or substitution).
	 * JaroWinklerDistance: The Jaro measure is the weighted sum of percentage of matched characters from each file and 
	 * transposed characters. Winkler increased this measure for matching initial characters.
	 */
	private int findMatchesWithDistanceAlgorithms(String token, int method){
		
		int term_min_len = Validator.getInt(4, skw.getPropertyValues().getProperty(PropertyKeys.TERM_MIN_LEN));

		for(int i=0; i<features.size(); i++){
			String term = features.get(i);
			if(term.length() > term_min_len){
				if(method == 1){
					int lev_threshold = 1;		//default - allows typo error up to 1 character difference						
					lev_threshold = Validator.getInt(lev_threshold, skw.getPropertyValues().getProperty(PropertyKeys.LEVENSHTEIN_THRESHOLD));
					int distance = StringUtils.getLevenshteinDistance(term, token);
					
					if(distance <= lev_threshold){
						return i;
					}
						
				}
				if(method == 2){
					double jaro_threshold = 0.9; //default:we need a very close similarity
					jaro_threshold = Validator.getDouble(jaro_threshold, skw.getPropertyValues().getProperty(PropertyKeys.JAROWINKLER_THRESHOLD));
					double sim = StringUtils.getJaroWinklerDistance(term, token);
					
					if(sim >= jaro_threshold){
						return i;
					}							
				}
				
			}else{
				//Then the term must be the same as the token for it to be acceptable
				if(term.equals(token))
					return i;
			}
			
		}
		
		return -1;
	}
	
	public List<Attribute> buildWekaAttributes(){
		// Dataset attributes
		List<Attribute> wDataset = new ArrayList<Attribute>();
		
		// Declare a nominal attribute along with its values
		 List<String> attr = new ArrayList<String>();
		 attr.add("yes");
		 attr.add("no");
		 for(int i=0; i<features.size(); i++){
			 Attribute attr_i = new Attribute("v"+i, attr);
			 wDataset.add(attr_i);
		 }		 
		 
		 // declare the class attribute along with its values
		 List<String> category = new ArrayList<String>();
		 category.add("0");
		 category.add("1");
		 Attribute cat = new Attribute("class", category);
		 
		 wDataset.add(cat);	
		 
		 return wDataset;
		 
	}
	
	/**
	 * @return Instances
	 */
	public Instances buildDataset(){
		
		ArrayList<Attribute> datasetAttr = (ArrayList<Attribute>) buildWekaAttributes();
		
		// create empty dataset
		Instances dataSet = new Instances("Rel", datasetAttr, datasetAttr.size());
		//set class index
		dataSet.setClassIndex(features.size());
		
		//populate training set
		for(int i=1; i<featureSpace.size(); i++){
			String[] inst = featureSpace.get(i);
			Instance instance = new DenseInstance(inst.length);
			for(int j=0; j<inst.length; j++){
				try{
					instance.setValue(datasetAttr.get(j), inst[j]);
				}catch(UnassignedDatasetException e){
					// do something here--log and report it
				}catch(IllegalArgumentException e){
					//do nothing here. Just move on
				}
				
			}
			//add each dataset instance
			dataSet.add(instance);
		}
		
		return dataSet;
	}

	/**
	 * @return the featureSpace
	 */
	public List<String[]> getFeatureSpace() {
		return featureSpace;
	}
	
	/**
	 * @return the features
	 */
	public List<String> getFeatures() {
		return features;
	}

	/**
	 * @param features the features to set
	 */
	public void setFeatures(List<String> features) {
		this.features = features;
	}

	/**
	 * @return the textDataStructure
	 */
	public List<TextDataStructure> getTextDataStructure() {
		return textDataStructure;
	}

	/**
	 * @return the textandClassification
	 */
	public TextDataStructure getTextandClassification(int index) {
		return textDataStructure.get(index);
	}
	
	/**
	 * @return the numTermFound
	 */
	public int getNumTermFound() {
		return termsFound.size();
	}

	/**
	 * @return the termsFound
	 */
	public Set<String> getTermsFound() {
		return termsFound;
	}

	/**
	 * @return the assetTermsFound
	 */
	public Set<String> getAssetTermsFound() {
		return assetTermsFound;
	}

	/**
	 * @return the controlTermsFound
	 */
	public Set<String> getControlTermsFound() {
		return controlTermsFound;
	}

	/**
	 * @return the directTermsFound
	 */
	public Set<String> getDirectTermsFound() {
		return directTermsFound;
	}

	/**
	 * @return the indirectTermsFound
	 */
	public Set<String> getIndirectTermsFound() {
		return indirectTermsFound;
	}

}
