/*
 * JiraSecPlugin 
 * Copyright 2016 Tosin Daniel Oyetoyan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package no.tosin.oyetoyan.plugins;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.util.JiraHome;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.comments.CommentManager;
import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.customfields.option.Options;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.user.ApplicationUser;

import no.tosin.oyetoyan.machinelearning.DeserializeModel;
import no.tosin.oyetoyan.plugins.exceptions.ExceptionStackTraceToString;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

/**
 * A JIRA listener using the atlassian-event library that listens to created/updated issues
 * and classifies them as security related or not 
 */

public final class IssueCreatedUpdatedListener implements InitializingBean, DisposableBean {

    private static final Logger log = LoggerFactory.getLogger(IssueCreatedUpdatedListener.class);
    
    private final EventPublisher eventPublisher;
    //private SecurityIssueClassifier securityClassifier;
    private SecurityKeyWords skw;
    //private String oldSecurityIssue;			//used to track user changes
    //private String oldSecurityImportance;
    private static final String SEP = System.getProperty("file.separator");
    //private Map<Long, IssueData> issueData;
    private DeserializeModel model;
    private static String lastSavedDesc = "";
    
    /**
     * Constructor.
     * @param eventPublisher injected {@code EventPublisher} implementation.
     */
    public IssueCreatedUpdatedListener(EventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;   
        skw = new SecurityKeyWords(true);
        model = DeserializeModel.getInstance(skw);
        //securityClassifier = new SecurityIssueClassifier(skw);
        //issueData = new HashMap<Long, IssueData>();
    }

    /**
     * Called when the plugin has been enabled.
     * @throws Exception
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        // register ourselves with the EventPublisher
        eventPublisher.register(this);
    }

    /**
     * Called when the plugin is being disabled or removed.
     * @throws Exception
     */
    @Override
    public void destroy() throws Exception {
        // unregister ourselves with the EventPublisher
        eventPublisher.unregister(this);
    }
    
    /**
     * Receives any {@code IssueEvent}s sent by JIRA.
     * @param issueEvent the IssueEvent passed to us
     */
    @EventListener
    public void onIssueEvent(IssueEvent issueEvent) {
        Long eventTypeId = issueEvent.getEventTypeId();
        Issue issue = issueEvent.getIssue();
        Long issueID = issue.getId();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        ApplicationUser user = issueEvent.getUser();
        //issueEvent.getUser();
        //ApplicationUser user = ComponentAccessor.getJiraAuthenticationContext().getUser();
        CommentManager commentManager = ComponentAccessor.getCommentManager();

        String securityIssue = Validator.getString("Security Issue", skw.getPropertyValues().getProperty(PropertyKeys.SECURITY_ISSUE));
        String securityMessage = Validator.getString("Security Description", skw.getPropertyValues().getProperty(PropertyKeys.SECURITY_DESC));
    	//String securityImportance = Validator.getString("Security Importance", skw.getPropertyValues().getProperty(PropertyKeys.SECURITY_IMPORTANCE));
        
        String description = "";
    	String summary = "";
    	StringBuilder commentBuilder = new StringBuilder();
    	
    	//Process comments, we might get important details from them
    	try{
    		List<Comment> comments = commentManager.getComments(issue);       	
        	for (Comment c : comments)
        		commentBuilder.append(c.getBody());
    	}catch(Exception e){ //this is okay but we should detect null dereferencing separately before catch
    		log.info(ExceptionStackTraceToString.getStackTrace(e));
    	}        	

    	try{
    		description = issue.getDescription().trim();        		
    	}catch(Exception e){
    		description = "";
    		log.info(e.toString());
    	}
    	
    	try{
    		summary = issue.getSummary().trim();        		
    	}catch(Exception e){
    		summary = "";
    		log.info(ExceptionStackTraceToString.getStackTrace(e));
    	}
    	//add comments to description
    	description = description.concat(" ").concat(commentBuilder.toString());
    	description = description.concat(" ").concat(summary);
    	
    	MutableIssue mutableIssue = getMutableIssue(issue);
    	
    	CustomField securityIssueCF = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName(securityIssue);
    	CustomField securityMessageCF = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName(securityMessage);
        
        // use a trained machine learning model
    	String result = model.classifyText(description);
    	String message = model.getMessage();
    	log.info("Number of Attributes="+model.getNumAttributes()+" Number found="+model.getNumTermFound());
    	log.info("Classification result: "+result);
    	//log.info(model.getMessage());
        
    	Option modelSecurityIssue = getCustomFieldOption(securityIssueCF, issue, result);
        
        // if issue is newly created just overwrite any preselected value
        if (eventTypeId.equals(EventType.ISSUE_CREATED_ID)){      	
        	//security issue
            mutableIssue.setCustomFieldValue(securityIssueCF, modelSecurityIssue);
            if(modelSecurityIssue.getValue().equals(skw.getPropertyValues().getProperty(PropertyKeys.OPTION_YES)))
            	mutableIssue.setCustomFieldValue(securityMessageCF, message);
            //this will store the newly set values to database. 
            issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
        }
        
        //if issue is updated,commented or comment edited change the classification. But accept user changes to the custom field if he disagrees with machine's classification :)
        if (eventTypeId.equals(EventType.ISSUE_UPDATED_ID) | eventTypeId.equals(EventType.ISSUE_DELETED_ID) | eventTypeId.equals(EventType.ISSUE_COMMENTED_ID) 
        		| eventTypeId.equals(EventType.ISSUE_COMMENT_EDITED_ID) | eventTypeId.equals(EventType.ISSUE_COMMENT_DELETED_ID)) {
            //notify that issue is being updated
        	//log.info("Issue Update Called: "+EventType.ISSUE_UPDATED_ID);
        	/**
             * Accept user selection if the custom field changes
             */
        	Option userSecurityIssue = (Option) securityIssueCF.getValue(mutableIssue);
        	
        	//we used the length of messages posted to detect changes
        	
        	try{
        		if(lastSavedDesc == ""){
        			if(!modelSecurityIssue.getValue().equals(skw.getPropertyValues().getProperty(PropertyKeys.OPTION_YES)))
            			message = "";
                    mutableIssue.setCustomFieldValue(securityIssueCF, modelSecurityIssue);
                    mutableIssue.setCustomFieldValue(securityMessageCF, message);
                    issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            	}else if(description.equals(lastSavedDesc)){
        			//log changes when user changed security Issue custom field selection
        			logChanges(issueID, summary, description, userSecurityIssue.getValue());

            		mutableIssue.setCustomFieldValue(securityIssueCF, userSecurityIssue); 
            		if(!userSecurityIssue.getValue().equals(skw.getPropertyValues().getProperty(PropertyKeys.OPTION_YES)))
            			message = "";
            		
            		mutableIssue.setCustomFieldValue(securityMessageCF, message);
                    issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            	}else{
            		// signifies changes in description or summary/comments. Therefore, reclassify - use model's result
            		if(!modelSecurityIssue.getValue().equals(skw.getPropertyValues().getProperty(PropertyKeys.OPTION_YES)))
            			message = "";
            		mutableIssue.setCustomFieldValue(securityIssueCF, modelSecurityIssue);
            		mutableIssue.setCustomFieldValue(securityMessageCF, message);
                    issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
            	}
        	}catch(Exception e){
        		//change to model's classification
        		if(!modelSecurityIssue.getValue().equals(skw.getPropertyValues().getProperty(PropertyKeys.OPTION_YES)))
        			message = "";
        		mutableIssue.setCustomFieldValue(securityIssueCF, modelSecurityIssue);
        		mutableIssue.setCustomFieldValue(securityMessageCF, message);
                issueManager.updateIssue(user, mutableIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
        		log.info(e.getMessage());
        	}

        }
        
        lastSavedDesc = description;
    }
    
    /**
     * Log user modified values
     * This is a shared resource. We need to synchronize it
     * @param description
     * @param summary
     * @param securityIssue
     * @param securityImportance
     */
    private synchronized void logChanges(Long issueID, String summary, String description, String securityIssue){
    	log.info("@ModifiedByUser"+"\t IssueID= "+issueID+"\t Issue Description= "+description+"\t Issue Summary="+summary+
    			"\t New Security Issue="+securityIssue);
    	//declaring resources (bw) that need to be closed automatically regardless of weather exception occurs or not
    	String STORAGE_PATH = ComponentAccessor.getComponentOfType(JiraHome.class).getHome()+SEP+
    			PropertyKeys.FOLDER+SEP;
    	String APP_JIRA_LOG = STORAGE_PATH+"usermodify-jira.log";
    	File file = new File(APP_JIRA_LOG);
    	if(!file.exists())
    		//create a new file in the root directory
			try {
				file = new File(APP_JIRA_LOG); //we know the path, non-existent path traversal threat
				file.createNewFile();
			} catch (IOException e) {
				log.info(ExceptionStackTraceToString.getStackTrace(e));
			}

    	try (BufferedWriter writer = new BufferedWriter(new FileWriter(file, true)); )
    	{
    		writer.write(issueID+","+summary+","+description+","+securityIssue.trim());
    		writer.newLine();
    		log.info("The usermodify-jira.log is located at: "+file.getCanonicalPath());

		} catch (IOException e) {
			log.info(ExceptionStackTraceToString.getStackTrace(e));
		}
    }
    
    private MutableIssue getMutableIssue(Issue issue) {
        MutableIssue mutableIssue;
        if (issue instanceof MutableIssue)   {
            mutableIssue = (MutableIssue)issue;
        } else {
            mutableIssue = ComponentAccessor.getIssueManager().getIssueObject(issue.getKey());
        }
        return mutableIssue;
    }
    
    /**
     * Get a custom field option from the value of the option.
     *
     * @param customField
     * @param issue
     * @param value
     * @return Option (customField option)
     */
    private Option getCustomFieldOption(CustomField customField, Issue issue, String value) {
        Option result = null;
        Options options = getCustomFieldOptions(customField, issue);
        //log.info("Options: "+options.get(0).getValue());
        Option opt = null;
        for (Object object : options) {
            opt = (Option) object;
            if (opt.getValue().equalsIgnoreCase(value)) {
                result = (Option) object;
                return result;
            }
        }
     
        return result;
    }
    
    /**
     * Get all options for a custom field.
     *
     * @param customField
     * @param issue
     * @return Options (customField options)
     */
    private Options getCustomFieldOptions(CustomField customField, Issue issue) {
        Options options = null;
        OptionsManager optionsManager = ComponentAccessor.getOptionsManager();
        if (null != customField) {
        	FieldConfig fieldConfig = customField.getRelevantConfig(issue);
        	
        	options = optionsManager.getOptions(fieldConfig);
        }
        
        return options;
    }
}