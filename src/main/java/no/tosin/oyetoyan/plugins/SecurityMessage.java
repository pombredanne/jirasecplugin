/**
 * 
 */
package no.tosin.oyetoyan.plugins;

import java.util.Set;

/**
 * @author tosindo
 *
 */
public class SecurityMessage {
	
	private String message;
	/**
	 * 
	 */
	public SecurityMessage(Set<String> assetTermsFound, Set<String> controlTermsFound,
			Set<String> directTermsFound, Set<String> indirectTermsFound, SecurityKeyWords skw) {
		
		securityMessage(assetTermsFound, controlTermsFound, directTermsFound, indirectTermsFound, skw);
		
	}
	
	private void securityMessage(Set<String> assetTermsFound, Set<String> controlTermsFound,
			Set<String> directTermsFound, Set<String> indirectTermsFound, SecurityKeyWords securityKeyWords){
		String msg = "";
		String ctrl = "You can downgrade the importance of this issue If you think this is a false alarm";
		boolean p = !assetTermsFound.isEmpty();
		boolean d = !directTermsFound.isEmpty();
		boolean c = !controlTermsFound.isEmpty();
		boolean i = !indirectTermsFound.isEmpty();
		
		//set the message in these order of importance
		
		if(p && d && c && i){
			msg = msg + securityKeyWords.getMessage().get("pdci");
			msg = msg.replace("@pii", assetTermsFound.toString());
			msg = msg.replace("@direct", directTermsFound.toString());
			msg = msg.replace("@control", controlTermsFound.toString());
			msg = msg.replace("@indirect", indirectTermsFound.toString());
			p = false;
			d = false;
			c = false;
			i = false;
		}
		if(p && d && c){
			msg = msg + securityKeyWords.getMessage().get("pdc");
			msg = msg.replace("@pii", assetTermsFound.toString());
			msg = msg.replace("@direct", directTermsFound.toString());
			msg = msg.replace("@control", controlTermsFound.toString());
			p = false;
			d = false;
			c = false;
		}
		if(d && c && i){
			msg = msg + securityKeyWords.getMessage().get("dci");			
			msg = msg.replace("@direct", directTermsFound.toString());
			msg = msg.replace("@control", controlTermsFound.toString());	
			msg = msg.replace("@indirect", indirectTermsFound.toString());
			d = false;
			c = false;
			i = false;
		}
		if(p && c && i){
			msg = msg + securityKeyWords.getMessage().get("pci");			
			msg = msg.replace("@pii", assetTermsFound.toString());
			msg = msg.replace("@control", controlTermsFound.toString());	
			msg = msg.replace("@indirect", indirectTermsFound.toString());
			p = false;
			c = false;
			i = false;
		}
		if(p && d){
			msg = msg + securityKeyWords.getMessage().get("pd");
			msg = msg.replace("@pii", assetTermsFound.toString());
			msg = msg.replace("@direct", directTermsFound.toString());
			p = false;
			d = false;
		}
		
		if(p && c){
			msg = msg + securityKeyWords.getMessage().get("pc");
			msg = msg.replace("@pii", assetTermsFound.toString());
			msg = msg.replace("@control", controlTermsFound.toString());
			p = false;
			c = false;
		}
		if(p && i){
			msg = msg + securityKeyWords.getMessage().get("pi");
			msg = msg.replace("@pii", assetTermsFound.toString());
			msg = msg.replace("@indirect", indirectTermsFound.toString());
			p = false;
			i = false;
		}
		if(d && c){
			msg = msg + securityKeyWords.getMessage().get("dc");
			msg = msg.replace("@direct", directTermsFound.toString());
			msg = msg.replace("@control", controlTermsFound.toString());
			d = false;
			c = false;
		}
		
		if(d && i){
			msg = msg + securityKeyWords.getMessage().get("di");
			msg = msg.replace("@direct", directTermsFound.toString());
			msg = msg.replace("@indirect", indirectTermsFound.toString());
			d = false;
			i = false;
		}
		if(c && i){
			msg = msg + securityKeyWords.getMessage().get("ci");
			msg = msg.replace("@control", controlTermsFound.toString());
			msg = msg.replace("@indirect", indirectTermsFound.toString());
			c = false;
			i = false;
		}
		
		if(p){
			msg = msg + securityKeyWords.getMessage().get("p");
			msg = msg.replace("@pii", assetTermsFound.toString());
		}
		if(d){
			msg = msg + securityKeyWords.getMessage().get("d");
			msg = msg.replace("@direct", directTermsFound.toString());
		}
		if(c){
			msg = msg + securityKeyWords.getMessage().get("c");
			msg = msg.replace("@control", controlTermsFound.toString());
		}
		if(i){
			msg = msg + securityKeyWords.getMessage().get("i");
			msg = msg.replace("@indirect", indirectTermsFound.toString());
		}
		
		msg = msg +" "+ctrl;
		
		message =  msg;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

}
