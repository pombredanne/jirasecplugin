package no.tosin.oyetoyan.plugins.exceptions;

public final class ExceptionStackTraceToString {
	
	private ExceptionStackTraceToString(){
		//disallow
	}
	
	public static String getStackTrace(Exception e){
		StringBuilder sb = new StringBuilder();
		StackTraceElement[] stElements = e.getStackTrace();
		for(int i=0; i<e.getStackTrace().length; i++){
			StackTraceElement st = stElements[i];
			sb.append(st.toString());
			sb.append("\n");
		}
		return sb.toString();
	}
}
