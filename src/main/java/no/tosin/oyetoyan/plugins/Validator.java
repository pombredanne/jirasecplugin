/*
JiraSecPlugin 
Copyright 2016 Tosin Daniel Oyetoyan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package no.tosin.oyetoyan.plugins;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import no.tosin.oyetoyan.plugins.exceptions.ExceptionStackTraceToString;

public final class Validator {
	
	private static final Logger logger = LoggerFactory.getLogger(Validator.class);
	
	private Validator() {
		// don't allow instantiating
	}
	
	public static boolean isNumber(String input){
		
		return input.matches("^\\d+$");
	}
	
	public static boolean isNumeric(String input){
		
		return input.matches("^[-+]?\\d+(\\.\\d+)?$");
	}

	public static boolean isAlphabets(String input){
		
		return input.matches("[a-zA-Z]+");
	}
	
	public static Integer getInt(int def, String var){
		int defb;
		try{
			defb = Integer.parseInt(var);
		}catch(NumberFormatException e){
			logger.info(ExceptionStackTraceToString.getStackTrace(e));
			defb = def;
		}
		
		return defb;
	}
	
	public static Double getDouble(double def, String var){
		double defb;
		try{
			defb = Double.parseDouble(var);
		}catch(NumberFormatException e){
			logger.info(ExceptionStackTraceToString.getStackTrace(e));
			defb = def;
		}
		return defb;
	}
	
	public static String getString(String def, String var){
		String defb = "";
		try{
			defb = String.valueOf(var);
		}catch(Exception e){
			logger.info(ExceptionStackTraceToString.getStackTrace(e));
			defb = def;
		}
		return defb;
	}
}
