/**
JiraSecPlugin 
Copyright 2016 Tosin Daniel Oyetoyan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/


package no.tosin.oyetoyan.plugins;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.util.JiraHome;

import no.tosin.oyetoyan.plugins.exceptions.ExceptionStackTraceToString;
import no.tosin.oyetoyan.plugins.exceptions.MinMaxException;
import no.tosin.oyetoyan.plugins.exceptions.SplitException;

/**
 * @author tosindo
 *
 */
public final class SecurityKeyWords implements ISecurityKeyWords {
	
	private static final Logger log = LoggerFactory.getLogger(SecurityKeyWords.class);
	private static final String SEP = System.getProperty("file.separator");
	
	private Set<String> controlTerms;
	private Set<String> directTerms;
	private Set<String> indirectTerms;
	private Set<String> piiTerms;
	private Set<String> nvdTerms;
	
	private Set<String> separators;
	private Set<String> stopWords;
	private Properties properties;
	
	private Map<String, Set<String>> securityDescription;
	private Map<String, String> message;
	private Map<Integer, String> importance;
	private boolean production = true;
	/**
	 * 
	 */
	public SecurityKeyWords(boolean production) {
		//create JiraSecPlugin directory under the jira home dir if it doesn't exist
		this.production = production;
		createDir();
		securityDescription = new HashMap<String, Set<String>>();
		try {
			feedbackMessage(loadKeyword("message.prop",false, false, false));
		} catch (SplitException e) {
			log.info(ExceptionStackTraceToString.getStackTrace(e));
		}
		try {
			importanceConfig(loadKeyword("importance.prop", false, false, false));
		} catch (MinMaxException e) {
			log.info(ExceptionStackTraceToString.getStackTrace(e));
		}
		controlTerms = loadKeyword("controlterms.prop", true, true, false);
		directTerms = loadKeyword("directterms.prop", true, true, false);
		indirectTerms = loadKeyword("indirectterms.prop", true, true, false);
		piiTerms = loadKeyword("piiterms.prop", true, true, false);
		nvdTerms = loadKeyword("nvdterms.prop", true, true, false);
		separators = loadKeyword("separators.prop", true, false, false);
		stopWords = loadKeyword("stopwords.prop", true, false, false);
		properties = loadPropertyFile();
		
		//create default model in the JiraSecPlugin dir
		loadKeyword("model.cls", false, false, true);
		loadKeyword("modeleval.cls", false, false, true);
	}
	
	private void createDir(){
		//during testing this path is not available, so we use working dir
		File jspDir = null;
		if(production){
			try{
				String STORAGE_PATH = ComponentAccessor.getComponentOfType(JiraHome.class).getHome()+SEP+PropertyKeys.FOLDER+SEP;
				jspDir = new File(STORAGE_PATH); //This is a valid known path and not input by a remote user. non-existent path traversal threat
			}catch(Exception e){
				log.info(e.getMessage());
				jspDir = new File(PropertyKeys.STORAGE_PATH_TEST); //test running
			}		
		}else
			jspDir = new File(PropertyKeys.STORAGE_PATH_TEST); //test running
		if(!jspDir.exists() && jspDir != null)
			try{
				if(jspDir.mkdirs()){
					jspDir.setWritable(true, true);	//make the path writable to the admin only
					jspDir.setExecutable(true, true); //make the path executable to the admin only
					jspDir.setReadable(true, false); //make the path readable to everyone
				}				
			}catch(SecurityException e){
				log.info(ExceptionStackTraceToString.getStackTrace(e));
			}
			
	}

	@Override
	public Set<String> controlTerms() {
		
		return Collections.unmodifiableSet(controlTerms);
	}

	@Override
	public Set<String> directTerms() {
		
		return Collections.unmodifiableSet(directTerms);
	}

	@Override
	public Set<String> indirectTerms() {
		
		return Collections.unmodifiableSet(indirectTerms);
	}

	@Override
	public Set<String> piiTerms() {
		
		return Collections.unmodifiableSet(piiTerms);
	}
	
	@Override
	public Set<String> separators() {
		
		return Collections.unmodifiableSet(separators);
	}
	
	@Override
	public Set<String> stopWords() {
		
		return Collections.unmodifiableSet(stopWords);
	}
	
	@Override
	public Properties getPropertyValues() {
		
		return (Properties) properties.clone();
	}
	
	private void feedbackMessage(Set<String> messages) throws SplitException {
		message = new HashMap<String, String>();
		for(String msg : messages){
			String[] tokens = msg.trim().split(";");
			this.message.put(tokens[0].toLowerCase(), tokens[1]);						
		}		
		
	}
	
	private void importanceConfig(Set<String> implevels) throws MinMaxException{
		importance = new HashMap<Integer, String>();
		for(String implevel : implevels){
			String[] tokens = implevel.trim().split("=");
			try{
				Integer level = Integer.parseInt(tokens[0]);
				if(level < 0)
					throw new MinMaxException("Invalid Importance Level: Value is less than 0");
				if(level > 5)
					throw new MinMaxException("Invalid Importance Level: Value is greater than 5");
				importance.put(level, tokens[1]);
			}catch(NumberFormatException e){
				log.info(ExceptionStackTraceToString.getStackTrace(e));
			}
		}
	}
	
	private Set<String> loadKeyword(String resourceFile, boolean usehelpermethod, boolean makeclones, boolean onlyCreateFile){
		Set<String> keyword = new HashSet<String>();
		String resourcePath = "config";
	
		//try to create the same files in the root directory for future maintenance purpose
		//program first look for the file in the jira_home directory, if it is not found, then it will try from the resource directory
		String resFile = resourcePath+SEP+resourceFile;
		String appResourceFile;
		if(production){
			try{
				String STORAGE_PATH = ComponentAccessor.getComponentOfType(JiraHome.class).getHome()+SEP+PropertyKeys.FOLDER+SEP;
				appResourceFile = STORAGE_PATH+resourceFile;
			}catch(Exception e){
				log.info(e.getMessage());
				appResourceFile = PropertyKeys.STORAGE_PATH_TEST+resourceFile;
			}
			
		}else
			appResourceFile = PropertyKeys.STORAGE_PATH_TEST+resourceFile;
		keyWordLoaderHelper2(resFile, appResourceFile, keyword, usehelpermethod, makeclones, true, onlyCreateFile);
		
		return keyword;
	}
	
	private void readerHelper(BufferedReader br, Set<String> keywords, boolean usehelpermethod, boolean makeclones, boolean changecase){
		String line = "";
		try {
			while((line=br.readLine()) != null){
				if(line.startsWith("#")) 
					continue;
				if(usehelpermethod)
					keyWordLoaderHelper(line, makeclones, keywords, changecase);
				else
					keywords.add(line);
			}
		} catch (IOException e) {
			log.info(ExceptionStackTraceToString.getStackTrace(e));
		}
	}
	
	/**
	 * Break the line into term, properties and threats and add properties and threats into 
	 * the securityDescription map
	 * ,;,;
	 */
	private void keyWordLoaderHelper(String line, boolean makeclones, Set<String> keywords, boolean changecase){
		String[] tokens = line.split(",");
		String term = "";
		try{
			term = tokens[0].trim();			
		}catch(IndexOutOfBoundsException e){
			log.info(ExceptionStackTraceToString.getStackTrace(e));
			term = line;	//If error, return line
		}
		if(changecase)	//we don't want to alter the .properties file values
			term = term.toLowerCase();
		keywords.add(term);
		//create compound terms by replacing term that has space with '-', '_', '' and add to list. Check for opposite too
		String term_hyphen = term;
		String term_underscore = term;
		String term_nospace = term;
		if(makeclones){
			//if term has space between two words, create modified clones with '', '-' and '_'
			if (term.contains(" ")){
				term_hyphen = term_hyphen.replaceAll(" ", "-");
				term_underscore = term_underscore.replaceAll(" ", "_");
				term_nospace = term_nospace.replaceAll(" ", "");
			}
			//if term has '-' between two words, create clones with '', ' ' and '_'
			if (term.contains("-")){
				term_hyphen = term_hyphen.replaceAll("-", " ");
				term_underscore = term_underscore.replaceAll("-", "_");
				term_nospace = term_nospace.replaceAll("-", "");
			}
			//if term has '_' between two words, create clones with '', ' ' and '-'
			if (term.contains("_")){
				term_hyphen = term_hyphen.replaceAll("_", " ");
				term_underscore = term_underscore.replaceAll("_", "-");
				term_nospace = term_nospace.replaceAll("_", "");
			}
			keywords.add(term_nospace);
			keywords.add(term_underscore);
			keywords.add(term_hyphen);
		}
		
		//add security properties and threats: property = tokens[1]
		if(tokens.length > 1){
			Set<String> secDesc = new HashSet<String>();
			String[] props = tokens[1].split(";");
			if(props.length > 0){
				for(String property : props)
					secDesc.add(property);
			}
							
			securityDescription.put(term.toLowerCase(), secDesc);
			securityDescription.put(term_hyphen.toLowerCase(), secDesc);
			securityDescription.put(term_underscore.toLowerCase(), secDesc);
			securityDescription.put(term_nospace.toLowerCase(), secDesc);
		}else{
			securityDescription.put(term.toLowerCase(), new HashSet<String>());
			securityDescription.put(term_hyphen.toLowerCase(), new HashSet<String>());
			securityDescription.put(term_underscore.toLowerCase(), new HashSet<String>());
			securityDescription.put(term_nospace.toLowerCase(), new HashSet<String>());
		}
		
	}
	
	private Properties loadPropertyFile(){
		Set<String> keyword = new HashSet<String>();
		//try to create the same files in the root directory for future maintenance purpose
		//program first look for the file in the root directory, if it is not found, then it will try from the resource directory
		String propFile = "security-issue-classifier.properties";
		String appPropFile;
		if(production){
			try{
				String STORAGE_PATH = ComponentAccessor.getComponentOfType(JiraHome.class).getHome()+SEP+"JiraSecPlugin"+SEP;
				appPropFile = STORAGE_PATH+propFile;
			}catch(Exception e){
				appPropFile = PropertyKeys.STORAGE_PATH_TEST+propFile;
			}
			
		}else
			appPropFile = PropertyKeys.STORAGE_PATH_TEST+propFile;
		keyWordLoaderHelper2(propFile, appPropFile, keyword, true, false, false, false);
		
		StringBuilder sb = new StringBuilder();
		for(String k : keyword){
			if(k.isEmpty()) 
				continue;
			sb.append(k);
			sb.append("\n");
		}
		
		Properties property = new Properties();
		InputStream inputStream = new ByteArrayInputStream(sb.toString().getBytes());
		
		if(null != inputStream)
			try {
				property.load(inputStream);				
			} catch (IOException e) {
				log.info(ExceptionStackTraceToString.getStackTrace(e));
			}
		
		return property;
	}
	
	private void keyWordLoaderHelper2(String resourceFile, String appResFile, Set<String> keyword, boolean usehelpermethod, 
			boolean makeclones, boolean makecase, boolean onlyCreateFile){
		//try to create the same files in the root directory for future maintenance purpose
		//program looks first for the file in the app directory, if it is not found, then it will try from the resource directory
		File f = new File(appResFile);	//Known path.(hardcoded) No path traversal threat	
		InputStream resStream = getClass().getClassLoader().getResourceAsStream(resourceFile);
		if(f.exists()){
			try(BufferedReader br = new BufferedReader(new FileReader(appResFile));)
			{
				readerHelper(br, keyword, usehelpermethod, makeclones, makecase);
			}catch(IOException e){
				log.info(ExceptionStackTraceToString.getStackTrace(e));
			}
		}else{
			//first time: copy the file from resource dir to the app_path (root) directory
			try {
				f = new File(appResFile);
				FileUtils.copyInputStreamToFile(resStream, f);
				
			} catch (IOException e) {
				log.info(ExceptionStackTraceToString.getStackTrace(e));
			}
			//now proceed
			try (				
					InputStream inputStream = getClass().getClassLoader().getResourceAsStream(resourceFile);
					BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
					)
			{
				if(!onlyCreateFile)
					readerHelper(br, keyword, usehelpermethod, makeclones, makecase);
				//br.close(); (try-with-resources from Java 1.7 ensures that resources are closed with or without operation failure)
				
			}catch(IOException e){
				log.info(ExceptionStackTraceToString.getStackTrace(e));
			}
		}
	}
	
	public Map<String, Set<String>> getSecurityDescription() {
		return securityDescription;
	}

	public Map<String, String> getMessage() {
		return message;
	}
	
	@Override
	public Map<Integer, String> importanceLevels() {
		
		return importance;
	}

	@Override
	public Set<String> nvdTerms() {
		
		return Collections.unmodifiableSet(nvdTerms);
	}

	/**
	 * @return the production
	 */
	public boolean isProduction() {
		return production;
	}
}
